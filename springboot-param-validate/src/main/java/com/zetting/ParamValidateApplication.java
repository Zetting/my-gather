package com.zetting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  启动类
 * @author oKong 
 *
 */
@SpringBootApplication
public class ParamValidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParamValidateApplication.class, args);
	}
}
