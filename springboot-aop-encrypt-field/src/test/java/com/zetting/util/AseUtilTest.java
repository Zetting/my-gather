package com.zetting.util;

import org.junit.Test;
import org.springframework.util.Assert;


/**
 * @author: linzetian
 * @date:2018/12/27
 */
public class AseUtilTest {

    @Test
    public void encrypt() {
        String key = "123456";
        String idCard = "420101196207212033";
        String encryIdCard = AseUtil.encrypt(idCard, key);
        String decryptIdCard = AseUtil.decrypt(encryIdCard, key);
        Assert.isTrue(idCard.equalsIgnoreCase(decryptIdCard), "解密后、解密前不一致");
    }

    @Test
    public void decrypt() {
    }
}